* [configure: fix -Wimplicit-function-declaration, -Wimplicit-int](https://github.com/tomac/yersinia/pull/77)
* [configure: fix -Wimplicit-function-declaration, -Wstrict-prototypes](https://github.com/tomac/yersinia/pull/76)
* https://src.fedoraproject.org/rpms/yersinia/c/bd12371e16e97e45be36065e77d117fe1c1f004e?branch=rawhide
