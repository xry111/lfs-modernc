* [build: Remove an implicit int](https://github.com/ivmai/cudd/pull/6) (fix for bundled copy of `cudd`)
* https://src.fedoraproject.org/rpms/yices/c/346da4370cf7f78415d19e7729131e101127f796?branch=rawhide
