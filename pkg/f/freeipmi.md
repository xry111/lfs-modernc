* [configure.ac: Fix -Wimplicit-function-declaration for printf](http://git.savannah.gnu.org/cgit/freeipmi.git/commit/?id=23b7fdb730d6d6e099f1c39549bf0f45a491efcf)
* [ipmi-sensors: Add missing header to file](http://git.savannah.gnu.org/cgit/freeipmi.git/commit/?id=66cf8d0bb7e47d3360389ce427a6303cc0b63d00)
* https://src.fedoraproject.org/rpms/freeipmi/c/1739ab8029a7361dbc923be7f1992dab01ab23a2?branch=rawhide
