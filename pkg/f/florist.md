* [configure: Avoid C89-only language features](https://github.com/AdaCore/florist/pull/10)
* [Fix warning about implicit declaration of toupper](https://github.com/AdaCore/florist/commit/e6c2f95ff8ae426c3d832f23aa80bcda82dcfa5c)
* https://src.fedoraproject.org/rpms/florist/c/6f2505db87bae9a9ca867952d061cbd405cee0a5?branch=rawhide
