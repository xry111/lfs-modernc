* [configure.ac: Improve C99 compatibility of HAVE_IOPRIO check](https://github.com/filebench/filebench/pull/160)
* https://src.fedoraproject.org/rpms/filebench/c/cba597ba5105c32fdb4244746a67fbfd17ccfa10?branch=rawhide
