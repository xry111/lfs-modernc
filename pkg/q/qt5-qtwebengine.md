* [C99 compatibility issue in libsync (impacts qt5-qtwebengine)](https://bugreports.qt.io/browse/QTBUG-111440)
* https://bugzilla.redhat.com/show_bug.cgi?id=2155642
* https://src.fedoraproject.org/rpms/qt5-qtwebengine/c/628adfbb0613c892b91689d0db85de631d04fdae?branch=rawhide
