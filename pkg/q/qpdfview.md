* [Please update bundled synctex](https://bugs.launchpad.net/qpdfview/+bug/2006446)
* [Fix implicit declaration of vasprintf](https://github.com/jlaurens/synctex/commit/2897465154892a7737dcc90e4d6a00a1d1b3922c)
* https://src.fedoraproject.org/rpms/qpdfview/c/975c06746b594af65b90c01d27ac1a2c5c3a0bd5?branch=rawhide
