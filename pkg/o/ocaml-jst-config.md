* [fix implicit-function-declaration error](https://github.com/janestreet/jst-config/commit/a5cddd0e657b9fc3f6775da8ebdaa6d25446b649)
* [Fix mkostemp discovery for C99 compatibility](https://github.com/janestreet/jst-config/pull/6)
* https://src.fedoraproject.org/rpms/ocaml-jst-config/c/e5a2d926d230cc89cb2749d127e0ba4aaf40aa9b?branch=rawhide
