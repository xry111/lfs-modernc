* [ocaml-newt: Add missing value return types in newt_int_stubs_by_hand.c](https://bugzilla.redhat.com/show_bug.cgi?id=2180106)
* https://src.fedoraproject.org/rpms/ocaml-newt/c/054df830a9464dcaa1928751c26f61e4eeb07bcd?branch=rawhide
