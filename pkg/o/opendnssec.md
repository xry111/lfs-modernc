* [common: add missing util.h header](https://github.com/opendnssec/opendnssec/commit/27290c5fcd065a5a857d37236e7f79121e303d0a)
* [enforcer: remove remove strptime build warning](https://github.com/opendnssec/opendnssec/commit/5422819c17c02e6069328b2f5e4bef6fe5c179df)
* [m4/acx_broken_setres.m4: Improve compatibility with C99](https://github.com/opendnssec/opendnssec/pull/843)
* https://src.fedoraproject.org/rpms/opendnssec/c/dd051b0e3c2fb2483f874384139e61b2ae11cf5c?branch=rawhide
