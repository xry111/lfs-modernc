* [configure.ac: Add <pty.h> include for openpty](https://github.com/openssh/openssh-portable/commit/40b0a5eb6e3edfa2886b60c09c7803353b0cc7f5)
* [Fix setres*id checks to work with clang-16.](https://github.com/openssh/openssh-portable/commit/32fddb982fd61b11a2f218a115975a87ab126d43)
* [Fix snprintf configure test for clang 15](https://github.com/openssh/openssh-portable/commit/5eb796a369c64f18d55a6ae9b1fa9b35eea237fb)
* [Fix function prototypes in configure](https://github.com/jbeverly/pam_ssh_agent_auth/pull/41)
* https://src.fedoraproject.org/rpms/openssh/c/d5591fb5ab788ab7266efa4b3df569d71bd54aee?branch=rawhide
* [Suggestion to use `u_char *` instead of `char *`](https://github.com/openssh-gsskex/openssh-gsskex/pull/19#pullrequestreview-1794730006)
* https://src.fedoraproject.org/rpms/openssh/c/87ae5d1d5a84ab6a3d1b573eb270a2c6bfb458de?branch=rawhide (fix for downstream-only patch `openssh-8.0p1-gssapi-keyex.patch`)
