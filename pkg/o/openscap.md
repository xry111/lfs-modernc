* [Fix compile error with future versions of gcc](https://github.com/OpenSCAP/openscap/pull/1922)
* https://src.fedoraproject.org/rpms/openscap/c/11558c84eb2418fb9fa4ba55fa61e63b1d2e8e8f?branch=rawhide
