* [heapify: Include <stdlib.h> in generated C code](https://github.com/cil-project/cil/pull/53)
* https://src.fedoraproject.org/rpms/ocaml-cil/c/f727325fd48eead0ad8d063ddca6ae03a251f2ef?branch=rawhide
