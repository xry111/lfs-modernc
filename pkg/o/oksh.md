* [configure: Add missing int return type to noreturncheck](https://github.com/ibara/oksh/pull/71)
* https://src.fedoraproject.org/rpms/oksh/c/5a37274a0b9cddd8da8940b3eedecf2bda041d51?branch=rawhide
* [Fix setresgid and setresuid test parameters. Found by vbcc.](https://github.com/ibara/oksh/commit/18d5ab4db23502554ada3c03138dcc83ca812303)
* https://src.fedoraproject.org/rpms/oksh/c/36a853663258a038ae3e4a549efd9622888039cf?branch=rawhide
