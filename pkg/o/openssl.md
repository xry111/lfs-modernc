* [openssl: C99 compatibility in downstream-only 0032-Force-fips.patch](https://bugzilla.redhat.com/show_bug.cgi?id=2152504)
* https://src.fedoraproject.org/rpms/openssl/c/106fe8964c342cc3c6699abf64f6ce70a478f505?branch=rawhide
