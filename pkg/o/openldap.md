* [ITS#10011 build: fix compatibility with stricter C99 compilers](https://git.openldap.org/openldap/openldap/-/commit/14f81bc47a4c462ccc609fce74feb014185e2bf9)
* https://src.fedoraproject.org/rpms/openldap/c/0e7065eaf28b6a6e208a63e655b8c22983db333e?branch=rawhide
