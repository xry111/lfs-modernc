* [Fixed some C warnings](https://github.com/mmottl/gsl-ocaml/commit/eec2ba17cba1022cb5fbba007e2173a260b6297b)
* https://src.fedoraproject.org/rpms/ocaml-gsl/c/c8bf8136ff1fa09a43d3468ed0d9eff1b96d07f4?branch=rawhide
