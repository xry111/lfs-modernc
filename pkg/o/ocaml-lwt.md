* [Prepare for stricter checking in GCC 14](https://github.com/ocsigen/lwt/pull/1004)
* https://src.fedoraproject.org/rpms/ocaml-lwt/c/66ac77d44bca97e946d5169cb479af4fd3808f7a?branch=rawhide
