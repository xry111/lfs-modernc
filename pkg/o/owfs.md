* [configure: Make nested function check compatible with C99 restrictions](https://github.com/owfs/owfs/pull/106)
* https://src.fedoraproject.org/rpms/owfs/c/20bacc81b3e20eac5c8aaf423fbd792ed64c60df?branch=rawhide
