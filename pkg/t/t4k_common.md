* [linebreak: Avoid implicit declaration of u8_mbtouc_unsafe function](https://github.com/tux4kids/t4kcommon/pull/16)
* https://src.fedoraproject.org/rpms/t4k_common/c/2b5ccab87c3c6e4726aff8b4eb5033575a4eae23?branch=rawhide
