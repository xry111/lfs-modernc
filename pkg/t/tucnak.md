* [tucnak 4.38](http://tucnak.nagano.cz/wiki/Changelog#4.38) fixed
  `configure.ac` to include `<gnu/libc-version.h>` for
  `gnu_get_libc_version` without documenting it in the changelog.
* [tucnak: C99 compatibility fix for the configure script](https://bugzilla.redhat.com/show_bug.cgi?id=2167084) (separate fix)
* https://src.fedoraproject.org/rpms/tucnak/c/5eb294e3401673fc97ba5d816bd3973365cd9c7c?branch=rawhide
