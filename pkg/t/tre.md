* [Merge pull request #19 from Bryan A. Jones <bjones AT ece.msstate.edu>, and include changes for Python's new unicode strings.](https://github.com/laurikari/tre/commit/996e3577755fef84b294926ea3b8987ef3d454db)
* https://src.fedoraproject.org/rpms/tre/c/be274d5da227455d8b99f061fa8ea9f1e29ef3e3?branch=rawhide
