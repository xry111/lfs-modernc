* [configure: Fix failing libpng and zlib detection](https://github.com/wxWidgets/wxWidgets/commit/f6a43a6074bf1429a6e34bd79da5bf9fe4df5a7c)
* [Remove wxPM, wxWidgets port to OS/2.](https://github.com/wxWidgets/wxWidgets/commit/01f9accd19755fdcd2a823834e5cd52d38f76376)
* https://src.fedoraproject.org/rpms/wxGTK3/c/e85d636fde20a4d02f821aa0a60317774b935ac8?branch=rawhide
