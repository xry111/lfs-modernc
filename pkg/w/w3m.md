* [acinclude.m4: fix configure tests broken with Clang 16 (implicit function declarations)](https://github.com/tats/w3m/commit/11fd7ffb21085611817db3c8570cccd60b4ea271)
* https://src.fedoraproject.org/rpms/w3m/c/0d98bdae0e6766c46ccbd79858660cc3ad80b268?branch=rawhide
