* [Fix implicit declaration configuration errors with Xcode 12](https://github.com/wxWidgets/wxWidgets/commit/da4b42688a6cd5444eefb91bf40183d88203ffca)
* https://src.fedoraproject.org/rpms/wxGTK/c/817512d9cc912ad3384da78ae4f7d5fca695cc42?branch=rawhide
