* [Fix C99 compatibility issue](https://sourceforge.net/p/wide-dhcpv6/patches/3/)
* https://src.fedoraproject.org/rpms/wide-dhcpv6/c/fb3d4a5bedb3ea06eaeeb8690e94ba0e489168fa?branch=rawhide
