* [Avoid implicit function declarations in tests, for C99 compatibility](https://github.com/rizsotto/Bear/pull/521)
* https://src.fedoraproject.org/rpms/bear/c/bf990708b63adc649d8d9d170c00871a881ffa17?branch=rawhide
