* https://bugzilla.redhat.com/show_bug.cgi?id=2179373
* [Port bundled Berkeley DB 4.8 configure script to C99 (#2179373)](https://src.fedoraproject.org/rpms/bitcoin-core/c/46ac13e3e6a6c1ce784d338d280df7487a5492b1?branch=rawhide)
