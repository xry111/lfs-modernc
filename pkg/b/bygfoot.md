* [Fix implicit-function-declaration warning](https://gitlab.com/bygfoot/bygfoot/-/commit/7c2f20b139a1497e7d2aecd645d4cae53a8ce07a)
* [Fix implicit-int warning](https://gitlab.com/bygfoot/bygfoot/-/commit/7dfabf380c0c63d3c5df0b773f76a1572789573a)
* https://src.fedoraproject.org/rpms/bygfoot/c/d14e8b3d5d887ed6eee5a80656721e2dee4f29ac?branch=rawhide
