* [[PATCH] aclocal.m4: fix -Wimplicit-function-declaration in dup2 check](https://lists.gnu.org/archive/html/bug-bash/2023-02/msg00000.html)
* https://src.fedoraproject.org/rpms/bash/c/08f64cbc4e6631776d593cc122010dbe73dfba23?branch=rawhide
