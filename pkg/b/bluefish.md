* [changes to compile on MacOS](https://sourceforge.net/p/bluefish/code/9007/)
* https://src.fedoraproject.org/rpms/bluefish/c/6769cd891b1714104e02ac140aadf6a564a49e36?branch=rawhide
* [Fix improper conversion of pointer to integer in bookmark.c](https://sourceforge.net/p/bluefish/tickets/80/)
* [fix for clang errors from Nuno Teixeira <nunotexbsd@users.sourceforge.net>](https://sourceforge.net/p/bluefish/code/8991/)
* https://src.fedoraproject.org/rpms/bluefish/c/b32f1dc846623fb7d72272bf179d748e65f8a262?branch=rawhide
