* [Resolve most compiler warnings (#10)](https://github.com/BuddiesOfBudgie/budgie-screensaver/commit/78799a0c4861d8e5577b80952c7407bd002f7f9a)
* [Dubious removal of setgroups from setuid.c](https://github.com/BuddiesOfBudgie/budgie-screensaver/issues/19)
* https://src.fedoraproject.org/rpms/budgie-screensaver/c/0edcea43a146b92a15d6495aa4e7ff241cb6faaf?branch=rawhide
