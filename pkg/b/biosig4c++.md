* [[b4c] address a number of minor compiler warnings (e.g. strptime)](https://sourceforge.net/p/biosig/code/ci/61b9bc4ed71a8275e4acdd3559dc2dc17064e821)
* [[b4c] address warnings on implicit-declaration](https://sourceforge.net/p/biosig/code/ci/f5aa44517d3f35041a3219eb97370b7ff1d3d91a)
* https://src.fedoraproject.org/rpms/biosig4c++/c/e861a99765d1d1b20bf938e487afce6d7681111e?branch=rawhide
