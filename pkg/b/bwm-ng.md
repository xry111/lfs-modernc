* Backported [upstream fix](https://github.com/vgropp/bwm-ng/pull/25)
* Currently [FTBFS](https://bugzilla.redhat.com/show_bug.cgi?id=2113135) in rawhide
* https://src.fedoraproject.org/rpms/bwm-ng/c/c02e197e9d2c864da262614265f00ab113601f40?branch=rawhide
