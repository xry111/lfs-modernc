* [fix build for clang16 with -Werror=implicit-int](https://sourceforge.net/p/gtkpod/patches/47/)
* https://src.fedoraproject.org/rpms/libgpod/c/00f9f6d93800be2a3087366389c4bd8741e9eaa3?branch=rawhide
