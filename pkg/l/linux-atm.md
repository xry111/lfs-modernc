* [C99 compatibility fix for src/maint/hediag.c](https://sourceforge.net/p/linux-atm/patches/9/)
* https://src.fedoraproject.org/rpms/linux-atm/c/b29a3493d25f4a3494933867b2f26b8de9df3f93?branch=rawhide
