* [libast.m4: Avoid implicit declaration of exit](https://github.com/mej/libast/pull/6)
* https://src.fedoraproject.org/rpms/libast/c/acb09adad2c3a2381388bd4bdc6d55797971b052?branch=rawhide
