* Upstream moved to the Meson build system, making autoconf fixes
  non-applicable there.
* [Use single header with GtkSource](https://gitlab.gnome.org/GNOME/libgda/-/commit/3415fc0a96bb71ac0f611f0737e839b5322a9174) (for `gtk_source_language_get_name` declaration)
* https://src.fedoraproject.org/rpms/libgda5/c/546553a0bb7dcb5bdc4ee1b839bc12d1788eed0f?branch=rawhide
* [GdaLdapConnection: fixed self-referencing issue](https://gitlab.gnome.org/GNOME/libgda/-/commit/d39a7c1d45e81dc32e5ccb42abff0655c44580c9) (for incompatible pointer type around in `gda_ldap_connection_declare_table)
* [gda-connection: avoids check internal connection data for virtual ones](https://gitlab.gnome.org/GNOME/libgda/-/commit/7abf8273674b42d9efc054f308c7e7a31120d03c) (`(GDestroyNotify) _gda_prepared_estatement_free` cast)
* https://src.fedoraproject.org/rpms/libgda5/c/904cff1701ed146260865ce655939e6c2d12578d?branch=rawhide
