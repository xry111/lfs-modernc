* [configure.ac: Improve C99 compatibility of pthread_create check](https://github.com/dinhvh/libetpan/pull/423)
* https://src.fedoraproject.org/rpms/libetpan/c/daa2a2c0f6b5f1f9d154c0aafd048aa60f3b80fc?branch=rawhide

