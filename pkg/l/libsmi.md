* [C99 compatibility fixes](https://lists.ibr.cs.tu-bs.de/hyperkitty/list/libsmi@ibr.cs.tu-bs.de/thread/MI6XPAR7JE2AY6UZIMXA3Q7JGD635JRR/)
* https://src.fedoraproject.org/rpms/libsmi/c/626632f933d7e80532fc2c37655272ed04dfbfa5?branch=rawhide
