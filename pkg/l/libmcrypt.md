* [Avoid implicit ints in mcrypt_symb.c for C99 compatibility](https://sourceforge.net/p/mcrypt/patches/15/)
* https://src.fedoraproject.org/rpms/libmcrypt/c/2dd664987ec9b3a59802e00de7b403b0528a02d5?branch=rawhide
* https://src.fedoraproject.org/rpms/libmcrypt/c/e02fbd614a5b7ba093e9e15ab322e7eb02d64e3b?branch=rawhide
