* [Avoid implicit function declarations in tests](https://github.com/haampie/libtree/pull/84)
* https://src.fedoraproject.org/rpms/libtree-ldd/c/104262d5727c53d076e92f69fea1b136c74085ee?branch=rawhide
