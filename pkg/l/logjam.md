* [Ensure that called functions are declared, for C99 compatibility](https://github.com/andy-shev/LogJam/pull/6)
* https://src.fedoraproject.org/rpms/logjam/c/bef92085a10e255c97608060303e5c114d97aaa1?branch=rawhide
