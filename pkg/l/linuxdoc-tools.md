* [[PATCH] sgmls: Avoid implicit ints/function declarations in configure](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=1034362)
* https://src.fedoraproject.org/rpms/linuxdoc-tools/c/de1d959218cae00c60e2b0f055c57829fe8923e0?branch=rawhide
