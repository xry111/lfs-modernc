* [configure.ac: Fix incompatible pointer type in zlib probe](https://github.com/lincity-ng/lincity-ng/pull/88)
* https://src.fedoraproject.org/rpms/lincity-ng/c/6bc84e496eaa80bb9f3e4899b731bbd51bbe8916?branch=rawhide
