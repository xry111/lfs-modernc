* [Avoid relying on C89 features in a few places](https://gitlab.com/samba-team/samba/-/merge_requests/2807) (submitted against Samba)
* https://src.fedoraproject.org/rpms/libtdb/c/46fd56a1493d3d410d49c4892c0912adac7561a7?branch=rawhide

