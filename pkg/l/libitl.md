* [Fix C99 compatibility issue](https://github.com/arabeyes-org/ITL/pull/51)
* https://src.fedoraproject.org/rpms/libitl/c/c6a9aadb53bbf25cb5214afe7794ce652f57aab5?branch=rawhide
