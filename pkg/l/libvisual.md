* [C99 compatibility fixes to avoid implicit function declarations](https://github.com/Libvisual/libvisual/pull/220)
  (for 0.4.x branch; the configure patch in Fedora is no longer
  needed since the switch to CMake)
* Upstream has switched to C++ for 0.5.x, which should eliminate
  issues around implicit function declarations (which are not
  supported by GNU C++ at all).  It appears to be difficult to build
  the current upstream sources on Fedora, so this could not be verfied.
* https://src.fedoraproject.org/rpms/libvisual/c/bcffd8eddbbcab5b00f930805396be5fdb55c5a7?branch=rawhide
