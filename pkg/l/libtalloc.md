* [Avoid relying on C89 features in a few places](https://gitlab.com/samba-team/samba/-/merge_requests/2807)
* https://src.fedoraproject.org/rpms/libtalloc/c/490dd061d3203d2efb48d6fe50512e5479a18bc2?branch=rawhide
