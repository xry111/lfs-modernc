* [meson: remove obsolete check for LOOP_CTL_GET_FREE](https://gitlab.com/libvirt/libvirt/-/commit/cbd6cf45ae702efc8e2b48485ff66ab06bd478d7)
* https://src.fedoraproject.org/rpms/libvirt/c/044c5b06651c92de46c3cbbd0df1c45fcb85bd30?branch=rawhide
