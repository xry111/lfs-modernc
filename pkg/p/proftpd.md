* [configure: Remove several implicit function declarations](https://github.com/proftpd/proftpd/pull/1667)
* https://src.fedoraproject.org/rpms/proftpd/c/4875b14e0f13765f677d8f39d17f2a8b94573289?branch=rawhide
* [configure: Use char ** for the iconv input argument](https://github.com/proftpd/proftpd/pull/1754)
* https://src.fedoraproject.org/rpms/proftpd/c/9d7d74d9c792a473280b814b302996f752801650?branch=rawhide
