* [Fix compilation on recent macOS.](https://github.com/tamentis/prwd/commit/cc8a7bfc6252a1e69d9ae9c2a76918155e9cd7f0)
* [configure: Additional C99 compatibility fixes](https://github.com/tamentis/prwd/pull/12)
* https://src.fedoraproject.org/rpms/prwd/c/865d38d9ee7230e3a944be2aade226e07e7e0cf8?branch=rawhide
