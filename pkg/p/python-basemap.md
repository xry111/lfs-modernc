* [Cython fixes for compatibility with current compilers](https://github.com/matplotlib/basemap/pull/595)
* https://src.fedoraproject.org/rpms/python-basemap/c/d98da59ce57d5fc024892f8534e56064592d3e40?branch=rawhide
