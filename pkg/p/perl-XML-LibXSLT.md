* [Avoid C implicit function declaration in Makefile.PL (C99 compat)](https://github.com/shlomif/perl-XML-LibXSLT/pull/7)
* https://src.fedoraproject.org/rpms/perl-XML-LibXSLT/c/53de30eed687cacc5655c88d2a8da5c41af4d068?branch=rawhide
