* [C99 compatibility fix](https://rt.cpan.org/Ticket/Display.html?id=145852)
* https://src.fedoraproject.org/rpms/perl-CGI-SpeedyCGI/c/24e3e12a0f97677f44c7634ae58adf850de42abd?branch=rawhide
