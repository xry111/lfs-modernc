* [Stop using pg_atoi, removed in PG 15](https://github.com/postgis/postgis/commit/f1e799c7ee4a76f0d1029f91983f9dd2e05db8fe)
* [Include <math.h> for isnan](https://github.com/postgis/postgis/pull/725)
* https://src.fedoraproject.org/rpms/postgis/c/4027e2bac25ab70c6813dca9901d5c86a6bbc9f7?branch=rawhide
