* [configure.ac: fix implicit function declaration in mail spool directory check](https://github.com/linux-pam/linux-pam/commit/f07fc9cac78851d3dfad1e8c54ee2671e6351853)
* https://src.fedoraproject.org/rpms/pam/c/bfff0f23a8a50f619b6fef4a150541381cf1d98f?branch=rawhide
