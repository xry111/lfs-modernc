* Upstream switched to CMake and no longer has an implicitly declared
  `close` function in `slirp/debug.c`.
* https://src.fedoraproject.org/rpms/pcem/c/d59e595de2c8a9f429f6951259bded2bdd539887?branch=rawhide
