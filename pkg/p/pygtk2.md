* [pygtk2: C99 incompatibilities due to pango internal function access (potential crashes on 64-bit)](https://bugzilla.redhat.com/show_bug.cgi?id=2190017)
* https://src.fedoraproject.org/rpms/pygtk2/c/930a15aa62e1bb2d443c652c2025347f01ff92ce?branch=rawhide
