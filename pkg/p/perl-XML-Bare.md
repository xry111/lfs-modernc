* [C99 compatibility issue due to missing <stdlib.h>](https://rt.cpan.org/Ticket/Display.html?id=145653)
* https://src.fedoraproject.org/rpms/perl-XML-Bare/c/858a9154d02251344b59d51ddf012322b8ee01cf?branch=rawhide
