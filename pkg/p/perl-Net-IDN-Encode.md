* [Bug #149108 for Net-IDN-Encode: [PATCH] use uvchr_to_utf8_flags instead of uvuni_to_utf8_flags (which is removed in perl 5.38.0)](https://rt.cpan.org/Public/Bug/Display.html?id=149108)
* https://src.fedoraproject.org/rpms/perl-Net-IDN-Encode/c/23ffbceda04b420ffd902b7ed3f58ab20d2cc759?branch=rawhide
