* [Fix errors  of type `error: implicit declaration of function 'nrn_random_arg' is invalid in C99` when running nrnivmodl on M1 Mac](https://github.com/NeuralEnsemble/PyNN/commit/297a96b5c6dd57cd6ab81b2b5ee4bb45b4570d91)
* https://src.fedoraproject.org/rpms/python-pynn/c/50f7e02064d297ed0770bd46c847ee4ebc2b79c0?branch=rawhide
