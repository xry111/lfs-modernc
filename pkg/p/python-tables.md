* [Fix some warnings on type mismatch](https://github.com/PyTables/PyTables/commit/44168c0d8e4c059ea51c8bc98a10784a74454b54)
* https://src.fedoraproject.org/rpms/python-tables/c/9324fa8086ddaae769d4cef16f9b41cdd7ba4c36?branch=rawhide
