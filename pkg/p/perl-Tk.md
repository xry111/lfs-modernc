* [Avoid implicit ints and implicit function declarations](https://github.com/eserte/perl-tk/pull/91)
* https://src.fedoraproject.org/rpms/perl-Tk/c/8c1ed1e6b9efa3e28e9858ffeb73b7bacad2ef8a?branch=rawhide
