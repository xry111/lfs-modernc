* [Fix type errors in configure script](https://sourceforge.net/p/ijbswa/patches/149/)
* https://src.fedoraproject.org/rpms/privoxy/c/049d95d7187a10acfb9836f5a273347eb07fa727?branch=rawhide
