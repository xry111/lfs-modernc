* [Fix implicit int compiler warning in configure check for PTHREAD_SCOPE_SYSTEM](https://github.com/python/cpython/commit/12078e78f6e4a21f344e4eaff529e1ff3b97734f)
* [gh-99086: Fix -Wstrict-prototypes, -Wimplicit-function-declaration warnings in configure.ac (#99406)](https://github.com/python/cpython/commit/fe35ca417fe81a64985c2b29e863ce418ae75b96e)
* [closes bpo-13497: Fix `broken nice` configure test. (GH-12041)](https://github.com/python/cpython/commit/90c6facebd5666fec85f125ee2795b48b30319a4)
* https://bugzilla.redhat.com/show_bug.cgi?id=2147519
* https://src.fedoraproject.org/rpms/python3.6/c/2dfbe7e5df8413e00220fee3e0268da72bab0b3b?branch=rawhide
