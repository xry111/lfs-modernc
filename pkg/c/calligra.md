* [Avoid implicit declaration of atoi in CMake check](https://invent.kde.org/office/calligra/-/merge_requests/81)
* https://src.fedoraproject.org/rpms/calligra/c/6a3af5b43f73f9b2bcd9efe21395e0303f999c09?branch=rawhide
