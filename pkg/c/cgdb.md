* [configure: Avoid implicit int in readline check](https://github.com/cgdb/cgdb/pull/338)
* https://src.fedoraproject.org/rpms/cgdb/c/e61bc01c9c0642db4efe7f9627b076979c0d6367?branch=rawhide
