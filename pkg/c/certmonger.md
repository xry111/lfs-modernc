* [Fixes for GCC 14 compatibility](https://pagure.io/certmonger/pull-request/265)
* https://src.fedoraproject.org/rpms/certmonger/c/c2f303f79bddeaebf007f27697f07479847ea717?branch=rawhide
