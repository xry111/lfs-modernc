* [Fix glibc feature macro handling for timegm](https://github.com/collectd/collectd/pull/4106)
* https://src.fedoraproject.org/rpms/collectd/c/33fa1bb551715adf60f4974a513ea6737d188277?branch=rawhide
* [configure.ac, src/nut.c: detect int types required by NUT API we build against](https://github.com/collectd/collectd/commit/d409ffa2a64cac3fc2abe2bb86ec3a80cb34d0a6)
* https://src.fedoraproject.org/rpms/collectd/c/121af79360b8ea048b8ab5cbd44e6b1d45bae52d?branch=rawhide
