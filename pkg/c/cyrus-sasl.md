* [Fix <time.h> check](https://github.com/cyrusimap/cyrus-sasl/commit/266f0acf7f5e029afbb3e263437039e50cd6c262)
* https://src.fedoraproject.org/rpms/cyrus-sasl/c/6bd250bfdc6fea23112775f667b634ccff40ab00?branch=rawhide
