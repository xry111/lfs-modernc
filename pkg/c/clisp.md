* [berkeley-db: Avoid failure of HAVE_DB_STAT_ACCEPT_TXN test in configure](https://gitlab.com/gnu-clisp/clisp/-/merge_requests/8)
* https://src.fedoraproject.org/rpms/clisp/c/0fb0f9b958537fcf8e5f4209e5865b9d0ea80a39?branch=rawhide
* [Fix pointer type mismatch in BDB:TXN-RECOVER](https://gitlab.com/gnu-clisp/clisp/-/merge_requests/9)
* https://src.fedoraproject.org/rpms/clisp/c/dbb1df508fafd18ed1178397b954c53c0aca8f6f?branch=rawhide
