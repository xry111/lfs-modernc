* [Improve C compatibility of LoadCommand tests](https://gitlab.kitware.com/cmake/cmake/-/merge_requests/9002)
* https://src.fedoraproject.org/rpms/cmake/c/2081d2ee8b21380acf6554163269d0af40613347?branch=rawhide (for -Wincompatible-pointer-types)
