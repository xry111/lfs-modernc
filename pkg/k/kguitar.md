* [Removed KDE3 legacy dir](https://github.com/pavelliavonau/kguitar/commit/4fc5dd7601fcac0c3477195bd70d4089066804c5) (upstream switched to CMake around that time, removing the problematic autoconf bits)
* https://src.fedoraproject.org/rpms/kguitar/c/d51c2052a59c690113eb640ea54b96e4abc1985c?branch=rawhide
