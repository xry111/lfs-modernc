* https://bugzilla.redhat.com/show_bug.cgi?id=2192886
* [perf build: Disable fewer bison warnings](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=ddc8e4c966923ad1137790817157c8a5f0301aec)
* https://src.fedoraproject.org/rpms/kernel-tools/c/a1949ada04de676d9fd8960b30a2c05e42690af7?branch=rawhide
