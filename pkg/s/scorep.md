* [configure: Fix type of flags argument to pthread_spin_init](https://gitlab.com/score-p/scorep/-/issues/1003)
* https://src.fedoraproject.org/rpms/scorep/c/0403fb34906dbcd0cfbcc41c4903618e076a54bd?branch=rawhide
