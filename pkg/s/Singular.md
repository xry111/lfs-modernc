* [omalloc/omAllocSystem.c: add another include for malloc.h.](https://github.com/Singular/Singular/commit/b6647a741b9091b82021ff46d4c112099d175d57)
* [addr2line check in omalloc/configure.ac always fails](https://github.com/Singular/Singular/issues/1174) (configure check also contains implicit function declarations, but it fails for other reasons, too)
* https://src.fedoraproject.org/rpms/Singular/c/f35612dc800be8ab79b73f049691f8f627e15818?branch=rawhide
