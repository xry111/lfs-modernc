* [Fixes for compiling with newer compilers e.g. clang-15](https://sourceforge.net/p/setserial/discussion/7060/thread/95d874c12c/?limit=25#1643)
* https://src.fedoraproject.org/rpms/setserial/c/d5842efee3ae2362df1d60fe1b3a0a893bc68a16?branch=rawhide
