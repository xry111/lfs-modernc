* [src/ssGtk.c: fix implicit function prototype declarations](https://github.com/trofi/ski/commit/241b7c6268b3adb43a92759850eeeec921ad17c8)
* [ssGtk.c: fix dataStart extern declaration](https://github.com/trofi/ski/commit/027b69d20b1e1c737bd41f0b936aae0055a1e8a1)
* https://src.fedoraproject.org/rpms/ski/c/73a73ca0bf7b8c313335f61ea7e10e01c37d5119?branch=rawhide
