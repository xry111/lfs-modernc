* [Rewrote music.c to support any number of decode libraries using a compiled-in plugin interface](https://github.com/libsdl-org/SDL_mixer/commit/a65a84adb8a898a1479e026c3e3520b8ce95b728) (removed problematic code)
* https://src.fedoraproject.org/rpms/SDL_mixer/c/d0e32e1d0c45241e4b77f02cedf2412a57d621ab?branch=rawhide
