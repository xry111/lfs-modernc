* (Merge pull request #324 in SUNDIALS/sunrepo from bugfix/fortran_name_mangling_test to develop)[https://github.com/LLNL/sundials/commit/edf10e9fa9cdf24cb76d4e4c75ae9f873b65bf9c]
* (Merge pull request #462 in SUNDIALS/sunrepo from feature/work-with-hypre-newer-than-2.22.0 to develop)[https://github.com/LLNL/sundials/commit/572517e6d7e11c38b78dfd896acff30943d6f1d7]
* https://src.fedoraproject.org/rpms/sundials2/c/b979199aa6c017f0d498dc77709ef0ffd334c00a?branch=rawhide
