* [stellarsolver: Not compatible with strict C99 mode](https://bugzilla.redhat.com/show_bug.cgi?id=2159395)
* [qsort_r detection relies on implicit function declaration](https://github.com/rlancaste/stellarsolver/issues/125)
* [Set build_type_safety_c to 0 (#2159395)](https://src.fedoraproject.org/rpms/stellarsolver/c/1ce245e315b8da5fa29cdac1a7ce7ed6eb358a4c?branch=rawhide)
