* [Avoid more implicit function declarations](https://github.com/jrincayc/ucblogo-code/pull/156)
* https://src.fedoraproject.org/rpms/ucblogo/c/dc47d79b012178c08778fa044850901d4a4526fe?branch=rawhide
