* [configure: Fix snprintf check for strict(er) C99 compilers](https://github.com/uim/uim/pull/187)
* https://src.fedoraproject.org/rpms/uim/c/a0dd9688317cf2ff7e9c4dea682df6356f5f4f99?branch=rawhide
