* [configure.ac: fix -Wimplicit-function-declaration (#242)](https://github.com/unrealircd/unrealircd/commit/d9d423ad7aef0a351448f0a39ae105060c514318)
* https://src.fedoraproject.org/rpms/unrealircd/c/08c2584a295f6d8f941648bac5226a45316b6ed0?branch=rawhide
