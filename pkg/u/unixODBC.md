* [Avoid implicit function declarations, for C99 compatibility](https://github.com/lurcher/unixODBC/pull/138)
* https://src.fedoraproject.org/rpms/unixODBC/c/ad00dfc6fe34f83115540173227131660d620002?branch=rawhide
