* [Reorder method order to avoid int-conversion compilation issue](https://github.com/MacPaw/XADMaster/pull/161)
* https://src.fedoraproject.org/rpms/unar/c/e744f4f23e636ce97e74478abf817409bef4e3b8?branch=rawhide
