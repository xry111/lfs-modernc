* [uwsgi: uwsgi-plugin-ruby is unusable, not ported to current Ruby](https://bugzilla.redhat.com/show_bug.cgi?id=2188210)
* [Fix Ruby/Rack plugin for Ruby >= 3.2](https://src.fedoraproject.org/rpms/uwsgi/c/8e28bafc5ef348fa589b3560d75bac22e6141aa6?branch=rawhide)
* [Fix incomplete patch for Ruby taint issue](https://src.fedoraproject.org/rpms/uwsgi/c/9a0d37a514eda1faae6ee76c6c4eb246c77b0871?branch=rawhide)
