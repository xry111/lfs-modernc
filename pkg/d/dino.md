* [dino: Build in C89 mode due to Vala compiler limitations](https://bugzilla.redhat.com/show_bug.cgi?id=2173174)
* [C99 compatibility of internal setters](https://discourse.gnome.org/t/c99-compatibility-of-internal-setters/13360)
* [valac does not respect internal header/vapi setting](https://gitlab.gnome.org/GNOME/vala/-/issues/358)
* https://src.fedoraproject.org/rpms/dino/c/2cc589288c6d9d107c4ea96f1c31655309d2bf9f?branch=rawhide