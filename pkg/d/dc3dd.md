* [C99 compatibility fix for the configure script](https://sourceforge.net/p/dc3dd/bugs/22/)
* https://src.fedoraproject.org/rpms/dc3dd/c/bf78b1c3f0b31ffdf1f70aebf8469c0c57160256?branch=rawhide
