* [glib-2.0: g_chdir is declared in <glib/gstdio.h>](https://gitlab.gnome.org/GNOME/vala/-/merge_requests/310)
* https://src.fedoraproject.org/rpms/vala/c/641f89612fd63b29c9b6c684e8857e51a6ef5d08?branch=rawhide
* Fix is in the vala package, but the build failure is an implicit `g_chdir` while building deja-dup.
