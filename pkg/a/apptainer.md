* [Fix fuse_mount function argument probing](https://github.com/vasi/squashfuse/pull/81) (bundled sources)
* https://src.fedoraproject.org/rpms/apptainer/c/dfe3385507de55eea9204416ae7506ec3fb75490?branch=rawhide
