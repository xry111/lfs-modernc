* [a2ps: FTBFS in rawhide, 64-bit and C99 compatibility issues](https://bugzilla.redhat.com/show_bug.cgi?id=2189133)
* https://src.fedoraproject.org/rpms/a2ps/c/e607f40fd0ffe16058d6e34f79c92e71b6bb89c6?branch=rawhide
