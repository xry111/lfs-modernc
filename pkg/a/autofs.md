* [[PATCH] autofs-5.1.8 - define LDAP_DEPRECATED during LDAP configure check](https://marc.info/?l=autofs&m=167840444620402)
* [autofs-5.1.8 - define LDAP_DEPRECATED during LDAP configure check](https://lore.kernel.org/autofs/87v8bt1eph.fsf@oldenburg.str.redhat.com/)
* https://src.fedoraproject.org/rpms/autofs/c/79c6ef3a0f52c31da62304be68ec6fcf0395f9b4?branch=rawhide
* https://src.fedoraproject.org/rpms/autofs/c/5ae410099ddf834644fd2f307a889239f04bbb65?branch=rawhide
* [[PATCH] configure: Fix type error for ldap_parse_page_control](https://lore.kernel.org/autofs/874jgf4qwu.fsf@oldenburg.str.redhat.com/)
* [[PATCH] Fix incompatible function pointer types in cyrus-sasl module](https://lore.kernel.org/autofs/878r5r4qy5.fsf@oldenburg.str.redhat.com/)
* https://src.fedoraproject.org/rpms/autofs/c/787a553722ec9aecf0c5108065bef9ef110d7639?branch=rawhide

