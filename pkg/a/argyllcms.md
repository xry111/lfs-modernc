* [Avoid build failure with future toolchain defaults](https://src.fedoraproject.org/rpms/argyllcms/c/53d1dcd9662a0e683e6304349ce68e16ab1c4c9d?branch=rawhide)
* `static eset = 0;` has already been fixed in `Argyll_dev_src.zip`
  (`Argyll_V2.3.2_beta`).
