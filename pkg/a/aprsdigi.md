* [Fix implicit ints and implicit function declarations (C99 compat)](https://github.com/n2ygk/aprsdigi/pull/11)
* https://src.fedoraproject.org/rpms/aprsdigi/c/c30a48b92dd248b8969f6fc115bd0658efc4f400?branch=rawhide
