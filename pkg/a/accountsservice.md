* [meson.build: fix -Wimplicit-function-declaration in configure tests for printf](https://gitlab.freedesktop.org/accountsservice/accountsservice/-/commit/453f893e3c38c209ae9dff47bca74ccb33a5bd34)
* [mocklibc: Fix compiler warning](https://gitlab.freedesktop.org/accountsservice/accountsservice/-/commit/da65bee12d9118fe1a49c8718d428fe61d232339)
* [user-manager: Fix another compiler warning](https://gitlab.freedesktop.org/accountsservice/accountsservice/-/commit/99aa57bfa59e2578c4ef47e84338f7de85c6f61b)
* https://src.fedoraproject.org/rpms/accountsservice/c/1c36351877a72330b6abce621dfce3c442ea12fc?branch=rawhide
