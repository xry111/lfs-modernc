* [0009554: C compatibility issue in localtime_r probe](https://tracker.ardour.org/view.php?id=9554)
* [Fix localtime_r check (#9554)](https://github.com/Ardour/ardour/commit/a8c26dbfa4e765e40bbf741637a3e383eebd770a)
* https://src.fedoraproject.org/rpms/ardour6/c/e2794d2ff44317ba8381e1acced9cbf5ceaaa628?branch=rawhide