* [Makefile.am: drop -Wno-implicit-function-declaration](https://github.com/radvd-project/radvd/pull/196)
* https://src.fedoraproject.org/rpms/radvd/c/309453adb321a100aa7f5fef2b6ff1e0fa25f660?branch=rawhide
* [configure.ac: Fix -Wint-conversion warnings](https://github.com/radvd-project/radvd/commit/5acb1c64766dfaf37c9745632f99feecf11d1403)
* https://src.fedoraproject.org/rpms/radvd/c/c77e9d991e93ed8bfacd17ef5b6613d5e7231b6b?branch=rawhide
