* [Define printf() before using it in a configure check](https://github.com/dajobe/rasqal/pull/11)
* https://src.fedoraproject.org/rpms/rasqal/c/f2a7ca62916698b2903b68e0b5f81de52dc5df05?branch=rawhide
* [configure: Fix incorrect argument type in gcry_md_open](https://github.com/dajobe/rasqal/pull/13)
* https://src.fedoraproject.org/rpms/rasqal/c/9874670cf25b7902cd0a02d0737441863f4fd731?branch=rawhide

