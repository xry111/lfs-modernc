* [configure: Avoid implicit int in inline keyword check](https://github.com/s-u/rJava/pull/305)
* https://src.fedoraproject.org/rpms/R-rJava/c/9f984908405342e87e3a244d3241cfa9bf972302?branch=rawhide
