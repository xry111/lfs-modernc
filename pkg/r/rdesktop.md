* [configure.ac: Fix statvfs64 check for C99 compatibility](https://github.com/rdesktop/rdesktop/pull/407)
* https://src.fedoraproject.org/rpms/rdesktop/c/12bc18a9162cb582b12832a51f4edc1e7f9a1e38?branch=rawhide
