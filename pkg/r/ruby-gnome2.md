* https://src.fedoraproject.org/rpms/ruby-gnome2/c/6db03f147d073029a584a41617020f8301b48796?branch=rawhide

In [upstream](https://github.com/ruby-gnome/ruby-gnome), many of the
C99 compatibility issues were fixed by migrating away from old-style
function declarations.  [This comment](https://github.com/ruby-gnome/ruby-gnome/pull/1528#issuecomment-1334935891)
explains why Fedora still packages this old version.
