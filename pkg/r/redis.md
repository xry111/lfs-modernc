* [deps/jemalloc: Do not force building in gnu99 mode](https://github.com/redis/redis/pull/11583)
* https://src.fedoraproject.org/rpms/redis/c/45aabba7d64fb49ab513836999bddcb07c928281?branch=rawhide
