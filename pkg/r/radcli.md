* [configure.ac use implicit return type for main in one of its checks, not C99 compliant.](https://github.com/radcli/radcli/issues/91)
* https://src.fedoraproject.org/rpms/radcli/c/cf34728cd985d0d7b9b9bff16c12294a5e2781c8?branch=rawhide
