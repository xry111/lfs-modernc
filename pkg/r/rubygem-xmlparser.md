* [rubygem-xmlparser: broken because it calls undefined ENC_TO_ENCINDEX function](https://bugzilla.redhat.com/show_bug.cgi?id=2185007)
* The package is already unusable because it calls an undefined function.
