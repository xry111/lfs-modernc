* [50641: use 'int main()' in test C-codes in configure](https://sourceforge.net/p/zsh/code/ci/ab4d62eb975a4c4c51dd35822665050e2ddc6918)
* https://src.fedoraproject.org/rpms/zsh/c/527f2cfb8126226100f39bf68b70ff15c1a4007e?branch=rawhide
* [[PATCH] C compatiblity fixes for the configure script (incompatible-pointer-types)](https://www.zsh.org/mla/workers/2023/msg01112.html)
* [52383: Avoid incompatible pointer types in terminfo global variable checks](https://github.com/zsh-users/zsh/commit/4c89849c98172c951a9def3690e8647dae76308f#commitcomment-135043161)
* https://src.fedoraproject.org/rpms/zsh/c/82b6281e8c2c5e61a9db8fc6510fc740e50c6be8?branch=rawhide
