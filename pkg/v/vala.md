* [Missing declarations of called C functions in [ModuleInit] function](https://gitlab.gnome.org/GNOME/vala/-/issues/1422)
* [codegen: Add declaration for register call of dynamic DBus interfaces](https://gitlab.gnome.org/GNOME/vala/-/commit/a902d7eae96a3f1ab0cb64f268443b23ee8ab45a)
* https://src.fedoraproject.org/rpms/vala/c/f57c17223228a505cb166e814f631d4fbb7fe24b?branch=rawhide
