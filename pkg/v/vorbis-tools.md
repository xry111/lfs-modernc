* [Fix C99 compatibility glitch in ogginfo/codec_skeleton.c](https://gitlab.xiph.org/xiph/vorbis-tools/-/merge_requests/6)
* https://src.fedoraproject.org/rpms/vorbis-tools/c/8ddea44dc96711504d41d32539f621027af407e7?branch=rawhide
