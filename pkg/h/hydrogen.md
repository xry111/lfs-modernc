* [rtclock: fix implicit sleep declaration](https://github.com/hydrogen-music/hydrogen/commit/f1aef07dbac734640dae28e99512e5e3f4b9a957)
* https://src.fedoraproject.org/rpms/hydrogen/c/3d8e05b39ec8743cfeb18e585d5a92b947f92f30?branch=rawhide
