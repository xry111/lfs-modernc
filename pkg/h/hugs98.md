* [hugs98: Needs C89 features (implicit function declarations, implicit ints) and C99 inlining mode](https://bugzilla.redhat.com/show_bug.cgi?id=2160645)
* [Set build_type_safety_c to 0 (#2160645)](https://src.fedoraproject.org/rpms/hugs98/c/36c5ca15c677ec74f3e5b866aa294a911a25e1e5?branch=rawhide)
