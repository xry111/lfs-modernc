* [Various C99 compatibility fixes](https://github.com/vanhauser-thc/thc-hydra/pull/817)
* https://src.fedoraproject.org/rpms/hydra/c/295e40350bf4a02e916892a9f5a9a16bcb70ad78?branch=rawhide
