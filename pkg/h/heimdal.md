* [Avoid implicit function declarations in configure, for C99 compatibility](https://github.com/heimdal/heimdal/pull/1085)
* https://src.fedoraproject.org/rpms/heimdal/c/a37b5d8b105138ad10c01085cb3b06e6943cfa12?branch=rawhide
