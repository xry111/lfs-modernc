* [ensure exit(2) prototype is present, clang-16 breaks otherwise](https://bazaar.launchpad.net/~mirabilos/jupp/trunk/revision/735)
* [C99 compatibility issue in setpgrp configure probe](https://bugs.launchpad.net/jupp/+bug/2006768)
* https://src.fedoraproject.org/rpms/jupp/c/f80e5983f8acb38201fe6ae074ba520039d840e4?branch=rawhide
