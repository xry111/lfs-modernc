* [libjulius: Fix incompatible types in POSIX threads configure checks](https://github.com/julius-speech/julius/pull/195)
* https://src.fedoraproject.org/rpms/julius/c/5325e94512a444bd7f05abc12308eb07d17276d7?branch=rawhide
