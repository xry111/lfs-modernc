* [C99 compatibility fixes for gnupg 1](https://lists.gnupg.org/pipermail/gnupg-devel/2023-January/035264.html)
* https://src.fedoraproject.org/rpms/gnupg1/c/e9338c60bb51e56718225232d5223da00eb0c521?branch=rawhide
