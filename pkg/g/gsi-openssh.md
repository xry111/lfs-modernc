* [configure.ac: Add <pty.h> include for openpty](https://github.com/openssh/openssh-portable/commit/40b0a5eb6e3edfa2886b60c09c7803353b0cc7f5)
* [Fix setres*id checks to work with clang-16.](https://github.com/openssh/openssh-portable/commit/32fddb982fd61b11a2f218a115975a87ab126d43)
* [Fix snprintf configure test for clang 15](https://github.com/openssh/openssh-portable/commit/5eb796a369c64f18d55a6ae9b1fa9b35eea237fb)
* https://src.fedoraproject.org/rpms/gsi-openssh/c/b6fce45c3ea8ecb69a130a9c8dc93094c929065b?branch=rawhide
