* [Add missing #include directives, for C99 compatibility](https://github.com/Jeija/gtkterm/pull/53)
* https://src.fedoraproject.org/rpms/gtkterm/c/c1a62d244efa19f05c4eadf819f9df09b22738dd?branch=rawhide
