* [Avoid implicit declaration of memset for C99 compatibility](https://savannah.nongnu.org/patch/index.php?10312)
* https://src.fedoraproject.org/rpms/gnokii/c/465933465a65a909ba1e4df03758c6f77de6ee76?branch=rawhide
