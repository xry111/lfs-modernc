* [Fix 'call to undeclared pthread_setname_np' errors in configure and cmake](https://github.com/ivmai/bdwgc/commit/4489757f1cc1fe4aa98d4c1ebbe0789b2b308e11)
* https://src.fedoraproject.org/rpms/gc/c/943dd49c9b70210b8b32199c6bfe648235a92700?branch=rawhide
