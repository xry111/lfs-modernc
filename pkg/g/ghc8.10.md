* [m4/fp_leading_underscore.m4: Avoid implicit exit function declaration](https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9394)
* https://src.fedoraproject.org/rpms/ghc8.10/c/3030d725192f0bc7745d6199004f96fd00e41357?branch=rawhide
