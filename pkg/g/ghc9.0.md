* [m4/fp_leading_underscore.m4: Avoid implicit exit function declaration](https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9394)
* https://src.fedoraproject.org/rpms/ghc9.0/c/4b84d37dfaa908ecad363aa561a5f571468becce?branch=rawhide
