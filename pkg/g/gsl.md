* [C99 compatibility fix for the configure script](https://savannah.gnu.org/bugs/index.php?63679)
* https://src.fedoraproject.org/rpms/gsl/c/eaa8503dbf9cb35d14004acbaa68a8c8c16fb051?branch=rawhide
