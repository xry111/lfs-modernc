* [settings: Fix incompatible pointer type warning](https://gitlab.gnome.org/GNOME/gnome-bluetooth/-/commit/c6f8fe96d9f9e85c306cdaebcd9227ecc247b09d)
* https://src.fedoraproject.org/rpms/gnome-bluetooth/c/fac9e6e262054369dc85f04ace1245a3f4aba73d?branch=rawhide
