* [m4/fp_leading_underscore.m4: Avoid implicit exit function declaration](https://gitlab.haskell.org/ghc/ghc/-/merge_requests/9394)
* https://src.fedoraproject.org/rpms/ghc/c/fbe8f0f8e341d636a91dc2d58d7033c0d288b516?branch=rawhide
