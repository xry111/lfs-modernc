* [Issue getdnsapi#526 Some gldns files need stdlib](https://github.com/getdnsapi/getdns/commit/9c076ca34b9569eb60861da9a99f895a49d5a7b4) (real bug fix: pointers returned from `malloc` are truncated)
* https://src.fedoraproject.org/rpms/getdns/c/bb69c5604b616aa1b04ee50200014d8e6ef6b981?branch=rawhide
