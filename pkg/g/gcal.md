* [C99 compatibility fix for the configure script](https://lists.gnu.org/archive/html/bug-gcal/2022-12/msg00000.html)
* https://src.fedoraproject.org/rpms/gcal/c/601028e3d18f98f1291a416dbefec7b4d16e43f4?branch=rawhide
