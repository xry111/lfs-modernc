* [Improve C99 compatibility of configure script](https://sourceforge.net/p/xbae/patches/3/)
* https://src.fedoraproject.org/rpms/xbae/c/7a28e8a24e967c9e4bb36646ea09062bb2da28ce?branch=rawhide
