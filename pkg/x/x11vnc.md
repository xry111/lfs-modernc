* [--without-v4l breaks configure script (with autoconf 2.71)](https://github.com/LibVNC/x11vnc/issues/221)
* [Conditional AC_CHECK_HEADER](https://lists.gnu.org/r/autoconf/2023-02/msg00001.html)
* https://src.fedoraproject.org/rpms/x11vnc/c/d741d166a9a7406077aa131f6018e1a9cb5a851e?branch=rawhide
