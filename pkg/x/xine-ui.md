* [Fix C compatibility issues in the configure script](https://sourceforge.net/p/xine/tickets/22/)
* https://src.fedoraproject.org/rpms/xine-ui/c/2346b4b3ca02a9d89a5feb5016300b06bd43f9c5?branch=rawhide
