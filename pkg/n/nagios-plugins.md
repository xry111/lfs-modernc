* [Fix implicit function declarations, strict prototypes (should resolve #670)](https://github.com/nagios-plugins/nagios-plugins/commit/8017ddc444b962a5df6f89a4a1cb4cb1a75d5b54)
* https://src.fedoraproject.org/rpms/nagios-plugins/c/936611dfe8ecea0b49a00744ba3ba780e92e7c7f?branch=rawhide
