* [fix largefile-check macro for largefile](https://github.com/Netatalk/netatalk/commit/a08e6137afaf8f6ec2139c250482c8515ceb3e2f)
* [C99 compatibility fixes](https://github.com/Netatalk/netatalk/pull/294)
* https://src.fedoraproject.org/rpms/netatalk/c/5869eacd55e53728e476e66b14f1eb8e1b68148f?branch=rawhide
