* [configure: Fix C99 vsnprintf check for stricter C99 compilers](https://github.com/NagiosEnterprises/nagioscore/pull/890)
* https://src.fedoraproject.org/rpms/nagios/c/ecad3c5b17a3f6238a58dfb380076c7ce702917f?branch=rawhide
