* [Avoid implicit function declarations in pthread_setname_np probe](https://github.com/MoarVM/MoarVM/pull/1730)
* https://src.fedoraproject.org/rpms/moarvm/c/58de3fa0108d46c0f790cc5ac23f187a9a968164?branch=rawhide
* [Fix incompatible function pointer types with clang16](https://github.com/MoarVM/MoarVM/commit/871f7bca4ffc2254f7b3d72e5d447120545f13ab)
* https://src.fedoraproject.org/rpms/moarvm/c/240dd95059dfb22c93e910e5d8dca59a30eb7aec?branch=rawhide
