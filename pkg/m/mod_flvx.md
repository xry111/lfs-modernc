* [Improve C99 compatibility](https://github.com/osantana-mirrors/mod_flvx/pull/2)
* https://src.fedoraproject.org/rpms/mod_flvx/c/e30047a82dad50da09ed156f28bddf6d6f8a57d7?branch=rawhide
