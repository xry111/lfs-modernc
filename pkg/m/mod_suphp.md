* [Switch back to a single Apache module tree.](https://github.com/lightsey/mod_suphp/commit/b96a30ce35d2ba4b3b9c5d22abdf1c02056436cb)
* [Apache 2.2 also needs the http_request header for ap_internal_redirect_handler](https://github.com/lightsey/mod_suphp/commit/af5efd00eaf51fc1caca91f38685e2733a7fd073)
* [Fix compiles with Apache 2.2.](https://github.com/lightsey/mod_suphp/commit/b3321a2005a4d632b100bfbcb7b82ec78a92fdec)
* https://src.fedoraproject.org/rpms/mod_suphp/c/ca80fc94b7bd509318070a4f20fc8373c061fe8a?branch=rawhide
