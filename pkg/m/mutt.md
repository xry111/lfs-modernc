* [configure.ac: Fix ICONV_NONTRANS probe](https://gitlab.com/muttmua/mutt/-/merge_requests/175)
* https://src.fedoraproject.org/rpms/mutt/c/e3bef1eb9143708200b55b7bf8724663becf9340?branch=rawhide
