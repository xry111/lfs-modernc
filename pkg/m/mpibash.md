* [circle-init: Avoid implicit declaration of execute_shell_function](https://github.com/lanl/MPI-Bash/pull/18)
* https://src.fedoraproject.org/rpms/mpibash/c/578f58514b594b736058833c11c6c6bc8162ced0?branch=rawhide
