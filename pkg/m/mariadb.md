* [rocksdb: Define _GNU_SOURCE during fallocate CMake probe](https://github.com/MariaDB/server/pull/2593)
* https://src.fedoraproject.org/rpms/mariadb/c/2efa260d360b2cb960b5adff9a2a1a71777f0561?branch=rawhide
