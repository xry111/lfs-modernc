* https://bugzilla.redhat.com/show_bug.cgi?id=2151172
* [Declare string_chomp in the installed header file](https://src.fedoraproject.org/rpms/saphire/c/2672fd62360a230a51b98879c5d2e2bd5ce16fff?branch=rawhide) (for `saphire`)
* https://src.fedoraproject.org/rpms/mfiler3/c/3920231f8efa61dbb191ce3766f088b953f974a6?branch=rawhide
