* [autoconf: Fix C99 compatibility issue in mc_GET_FS_INFO check](https://midnight-commander.org/ticket/4438)
* https://src.fedoraproject.org/rpms/mc/c/8f0d8878c845014935aa121c44d98e864d0a28c7?branch=rawhide
