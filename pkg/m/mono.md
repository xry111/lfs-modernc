* [configure: Fix type errors in __thread test](https://github.com/mono/mono/pull/21730)
* https://src.fedoraproject.org/rpms/mono/c/e733e87b0ce49fad204d9ca6aa1e4525fff9420a?branch=rawhide
